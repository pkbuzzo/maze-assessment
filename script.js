const mazeBoard = [
  "WWWWWWWWWWWWWWWWWWWWW",
  "W         W       W W",
  "WW WW WWW WWW WWW W W",
  "W   W   W   W   W   W",
  "WWW WWW W W WWW WWW W",
  "W     W   W   W   W W",
  "W WWW WWWWW WWWWW W W",
  "W W   W   W W     W W",
  "W WWWWW W W W WWWWW F",
  "S     W W W W     WWW",
  "WWWWW W W WWWWWWW   W",
  "W     W W   W   W W W",
  "W WWWWW W WWW W WWW W",
  "W       W     W     W",
  "WWWWWWWWWWWWWWWWWWWWW",
]; //store cells in array to use with my classAssignment function


const container = document.getElementById("gameBoard");
let wallHitCount = 0;



for (let rowIndex = 0; rowIndex < 15; rowIndex++) { //iterate through number of items in mazeBoard
const rowDiv = document.createElement("div"); //create div for each item in mazeBoard
rowDiv.classList.add('row'); //assign class for styling
container.appendChild(rowDiv); //append to container div

for (let spotIndex = 0; spotIndex < 21; spotIndex++) {//run for loop for amount of characters within each item in mazeBoard
  const spotDiv = document.createElement("div");
  spotDiv.classList.add("spot"); //assign class for styling each spot within rowDiv
  spotDiv.id = rowIndex + "-" + spotIndex; //create ID for each spot in maze to be used 

  function classAssignment() { //assign different colors to different spots, based off what their text equivalent inside mazeBoard
    if(mazeBoard[rowIndex][spotIndex] == "W")  {
       spotDiv.classList.add("wall");
    } else if (mazeBoard[rowIndex][spotIndex] == " ") {
      spotDiv.classList.add("floor");
    } else if (mazeBoard[rowIndex][spotIndex] == "S") {
      spotDiv.classList.add("start"); //make green class
    } else {
      spotDiv.classList.add("exit"); //make red class, only one space left 
    }            
  }
  classAssignment(); 

  rowDiv.appendChild(spotDiv); //append spots to their rowDiv's
  }
}



const begin = document.getElementById("9-0"); 

const playerDiv = document.createElement("div"); 

playerDiv.classList.add("player");

begin.appendChild(playerDiv); //create playerDiv that will move over the rest of the maze, use .appendChild



document.addEventListener('keyup', changeCoords); //event listener is activated on the release of chosen key



function changeCoords(event) {
  var moveSpot = playerDiv.parentElement.id; //grab location of spot div that playerDiv is on top of, using coordinates located in ID
  var playerSpot = moveSpot.split("-"); //organzie coordinates 
  if (event.key == "ArrowDown") {
    spot = Number(playerSpot[1]) + 0; //none
    row = Number(playerSpot[0]) + 1; //add to
  } else if (event.key == "ArrowUp") {
    spot = Number(playerSpot[1]) + 0; //none
    row = Number(playerSpot[0]) - 1; //subtract
  } else if (event.key == "ArrowLeft") { 
    spot = Number(playerSpot[1]) - 1; //subtract
    row = Number(playerSpot[0]) + 0; //none
}   else if (event.key == "ArrowRight") {
    spot = Number(playerSpot[1]) + 1; //add to
    row = Number(playerSpot[0]) + 0;
}   //update row and spot accordingly, based off the event.key of the 'arrow' key that was used

const futureSpot = row + "-" + spot;
const hereNow = document.getElementById(futureSpot);

     
if (hereNow.classList.contains("wall")) {
  wallHitCount += 1
  if (wallHitCount >= 3) {
    alert('Whoops! You hit the wall too many times! Refresh the page and start again!')
    return;
  } else {
    return;
  }
}
  

hereNow.appendChild(playerDiv); //append updated move to playerDiv

function checkForWin() { //check to see if the user is able to move to the 'finish' spot
  if (hereNow === document.getElementById('8-21')) { //8-20 is coordinates of spot with 'exit' class
    alert('You Win!'); //alert if winner

}
}
}

